package datasourse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import parser.SrumEnum;
import model.Story;
import model.Task;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * JDBC database
 * Variables passed as arguments to prepared statements will automatically be escaped by the JDBC driver.
 * maximum length for ID is 255
 * maximum length for description is 32672 
 */
public class JDBCDataSource implements DataSource {
	
	private Connection con;
	
	public JDBCDataSource(Connection argCon){
		con = argCon;
	}
	
	private void checkID(String str) throws ScrumException{
		if(str.length()>=255) throw new ScrumException(ExcptionType.MAXIMUM_ID_LEN);			
	}
	
	private void checkDescription(String str) throws ScrumException{
		if(str.length()>=32672) throw new ScrumException(ExcptionType.MAXIMUM_DES_LEN);			
	}

	@Override
	public void createStory(String storyid, String description, SrumEnum status) throws ScrumException {
		checkID(storyid);
		checkDescription(description);
		try {
			String query = "INSERT INTO Stories VALUES (?,?,?)";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, storyid);
			prepStmt.setString(2, description);
			prepStmt.setString(3, status.toString());
			prepStmt.execute();
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public List<Story> getStories() throws ScrumException {
		try {
			String query = "SELECT * FROM Stories";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.execute();
			ResultSet rs = prepStmt.getResultSet();
			List<Story> ret = new ArrayList<Story>();
	        while (rs.next()) {
	        	ret.add(new Story(rs.getString(1),rs.getString(2),SrumEnum.valueOf(rs.getString(3))));
	        }
			return ret;
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public Story getStory(String storyid) throws ScrumException {
		checkID(storyid);
		try {
			String query = "SELECT * FROM Stories WHERE StoryID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, storyid);
			prepStmt.execute();
			ResultSet rs = prepStmt.getResultSet();
	        if (rs.next()) {
	        	return new Story(rs.getString(1),rs.getString(2),SrumEnum.valueOf(rs.getString(3)));
	        }
			return null;
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public void deleteStory(String storyid) throws ScrumException {
		checkID(storyid);
		try {
			String query = "DELETE FROM Stories WHERE StoryID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, storyid);
			prepStmt.execute();
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public void updateStory(String storyid, SrumEnum status) throws ScrumException {
		checkID(storyid);
		try {
			String query = "UPDATE Stories SET Status= ? WHERE StoryID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, status.toString());
			prepStmt.setString(2, storyid);
			prepStmt.execute();
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public void createTask(String storyid, String taskid, String description,
			SrumEnum column) throws ScrumException {
		checkID(storyid);
		checkID(taskid);
		checkDescription(description);
		try {
			String query = "INSERT INTO Tasks VALUES (?,?,?,?)";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, storyid);
			prepStmt.setString(2, taskid);
			prepStmt.setString(3, description);
			prepStmt.setString(4, column.toString());
			prepStmt.execute();
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public List<Task> getTasks(String storyid) throws ScrumException {
		checkID(storyid);
		try {
			String query = "SELECT * FROM Tasks WHERE StoryID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, storyid);
			prepStmt.execute();
			ResultSet rs = prepStmt.getResultSet();
			List<Task> ret = new ArrayList<Task>();
	        while (rs.next()) {
	        	ret.add(new Task(rs.getString(1),rs.getString(2),rs.getString(3),SrumEnum.valueOf(rs.getString(4))));
	        }
			return ret;
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public Task getTask(String storyid, String taskid) throws ScrumException {
		checkID(storyid);
		checkID(taskid);
		try {
			String query = "SELECT * FROM Tasks WHERE StoryID= ? AND TaskID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, storyid);
			prepStmt.setString(2, taskid);
			prepStmt.execute();
			ResultSet rs = prepStmt.getResultSet();
	        if (rs.next()) {
	        	return new Task(rs.getString(1),rs.getString(2),rs.getString(3),SrumEnum.valueOf(rs.getString(4)));
	        }
			return null;
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public void deleteTask(String storyid, String taskid) throws ScrumException {
		checkID(storyid);
		checkID(taskid);
		try {
			String query = "DELETE FROM Tasks WHERE StoryID= ? AND TaskID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, storyid);
			prepStmt.setString(2, taskid);
			prepStmt.execute();
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public void moveTask(String storyid, String taskid, SrumEnum column) throws ScrumException {
		checkID(storyid);
		checkID(taskid);
		try {
			String query = "UPDATE Tasks SET TaskColumn= ? WHERE StoryID= ? AND TaskID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, column.toString());
			prepStmt.setString(2, storyid);
			prepStmt.setString(3, taskid);
			prepStmt.execute();
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public void updateTask(String storyid, String taskid, String description) throws ScrumException {
		checkID(storyid);
		checkID(taskid);
		checkDescription(description);
		try {
			String query = "UPDATE Tasks SET Description= ? WHERE StoryID= ? AND TaskID= ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.setString(1, description);
			prepStmt.setString(2, storyid);
			prepStmt.setString(3, taskid);
			prepStmt.execute();
		} catch (SQLException e) {
			throw new ScrumException(ExcptionType.SQL_EXCEPTION);
		}
	}

	@Override
	public void close() throws SQLException {
		con.close();
	}

	@Override
	public void init(boolean reset) throws SQLException {
		if(reset){
			String query = "DROP TABLE Stories";
			PreparedStatement prepStmt = con.prepareStatement(query);
			prepStmt.execute();
			query = "DROP TABLE Tasks";
			prepStmt = con.prepareStatement(query);
			prepStmt.execute();
		}
		String query = "CREATE TABLE Stories (StoryID varchar(255) PRIMARY KEY, Description varchar(32672), Status varchar(255))";
		PreparedStatement prepStmt = con.prepareStatement(query);
		prepStmt.execute();
		query = "CREATE TABLE Tasks (StoryID varchar(255), TaskID varchar(255), Description varchar(32672), TaskColumn varchar(255), PRIMARY KEY (StoryID,TaskID))";
		prepStmt = con.prepareStatement(query);
		prepStmt.execute();	
	}

}
