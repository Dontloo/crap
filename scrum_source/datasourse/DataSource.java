package datasourse;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import parser.SrumEnum;
import model.Story;
import model.Task;
import execption.ScrumException;

public interface DataSource {
	public void createStory(String storyid, String description, SrumEnum status) throws ScrumException;
	public List<Story> getStories() throws ScrumException;
	public Story getStory(String storyid) throws ScrumException;
	public void deleteStory(String storyid) throws ScrumException;
	public void updateStory(String storyid, SrumEnum status) throws ScrumException;
	public void createTask(String storyid, String taskid, String description, SrumEnum status) throws ScrumException;	
	public List<Task> getTasks(String storyid) throws ScrumException;
	public Task getTask(String storyid, String taskid) throws ScrumException;
	public void deleteTask(String storyid, String taskid) throws ScrumException;
	public void moveTask(String storyid, String taskid, SrumEnum status) throws ScrumException;
	public void updateTask(String storyid, String taskid, String description) throws ScrumException;
	public void close() throws IOException, SQLException;
	public void init(boolean reset) throws SQLException;
}
