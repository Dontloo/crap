package datasourse;

/**
 * @author Donglu
 * Mode for loading a data source
 * INIT: initialize when loading (barely used)
 * CONT: continue with existing data
 * RESET: reset when loading
 */
public enum Mode {
	INIT, CONT, RESET
}