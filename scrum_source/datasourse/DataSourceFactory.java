package datasourse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import parser.SrumEnum;
import execption.ExcptionType;
import execption.ScrumException;


/**
 * @author Donglu
 * for creating data sources
 * 
 * Data source types, support "in memory" and "Derby" currently
 * "in memory": memory based data source, will serialize the data to local file system.
 * "Derby": a relational database implemented entirely in Java.
 * 
 * Mode for loading a data source
 * INIT: initialize when loading (barely used)
 * CONT: continue with existing data
 * RESET: reset when loading
 */
public class DataSourceFactory {
	public static DataSource createDataSource(String name, String url, Mode mode) throws IOException, ClassNotFoundException, ScrumException, SQLException{
		switch (name) {
		case "in_memory":	return createInMemoryDataSource(url, mode);
		case "derby":	return createJDBCDataSource(url, mode);
		}
		throw new ScrumException(ExcptionType.DATASOURCE_NOT_FOUND);
	}
	
	/**
	 * if mode is INIT, initialize without resetting
	 * if mode is RESET, initialize with resetting
	 * if mode is CONT, don't need to initialize
	 */
	public static DataSource createJDBCDataSource(String url, Mode mode) throws SQLException{
		Connection con = DriverManager.getConnection(url);
		JDBCDataSource ret = new JDBCDataSource(con);
		if(mode.equals(Mode.INIT)){
			ret.init(false);
		}else if(mode.equals(Mode.RESET)){
			ret.init(true);
		}		
		return ret;
	}

	/**
	 * if mode is CONT, try to load the serialization file.
	 * otherwise, don't need to load. 
	 * 
	 * note that because the program will end only when user types EOF from console,
	 * it happens often that the program is closed unexpectedly,
	 * which will result in a irregular serialization file,
	 * so here if we can't read from the existing serialization file,
	 * we choose to reset the data source automatically, instead of throwing up an exception.
	 */
	public static DataSource createInMemoryDataSource(String path, Mode mode){
		try {
			File f = new File(path);
			if(mode.equals(Mode.CONT)&&f.exists()){			
				FileInputStream fis = new FileInputStream(f);
	            ObjectInputStream ois = new ObjectInputStream(fis);
	            InMemoryDataSource dataSource = (InMemoryDataSource) ois.readObject();
	            ois.close();
	            fis.close();
				return dataSource;
			}
		} catch (Exception e) {
			// prompt
		}
		return new InMemoryDataSource(path);
	}
	

	/**
	 * for testing
	 */
	public static void main(String[] args) throws ClassNotFoundException, IOException, ScrumException, SQLException {
		DataSource dataSource = DataSourceFactory.createDataSource("derby","~",Mode.CONT);
		dataSource.getStories();
		dataSource.createTask("my_stroy", "your_task0", "this is an endless task.", SrumEnum.TO_DO);
		dataSource.getTasks("my_stroy");
//		DriverManager.getConnection("jdbc:derby:;shutdown=true");
	}
}
