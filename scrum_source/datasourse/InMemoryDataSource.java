package datasourse;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import parser.SrumEnum;
import model.Story;
import model.Task;

/**
 * @author Donglu
 * in-memory data source,
 * will serialize to local file system if the close() method is invoked.
 */
public class InMemoryDataSource implements DataSource, Serializable {

	private static final long serialVersionUID = 7128672759361058644L;
	private Map<String,Story> stories;
	private Map<String,Map<String,Task>> tasks;
	private String path;
	
	public InMemoryDataSource(String argPath){
		path = argPath;
		stories = new TreeMap<String,Story>();
		tasks = new TreeMap<String,Map<String,Task>>();
	}

	@Override
	public void createStory(String storyid, String description, SrumEnum status) {
		stories.put(storyid, new Story(storyid,description,status));
		tasks.put(storyid, new TreeMap<String,Task>());
	}

	@Override
	public List<Story> getStories() {
		return new ArrayList<Story>(stories.values());
	}

	@Override
	public Story getStory(String storyid) {
		return stories.get(storyid);
	}

	@Override
	public void deleteStory(String storyid) {
		stories.remove(storyid);
		tasks.remove(storyid);
	}

	@Override
	public void updateStory(String storyid, SrumEnum status) {
		stories.get(storyid).setStatus(status);
	}

	@Override
	public void createTask(String storyid, String taskid, String description,
			SrumEnum column) {
		tasks.get(storyid).put(taskid, new Task(storyid,taskid,description,column));

	}

	@Override
	public List<Task> getTasks(String storyid) {
		return new ArrayList<Task>(tasks.get(storyid).values());
	}

	@Override
	public Task getTask(String storyid, String taskid) {
		return tasks.get(storyid).get(taskid);
	}

	@Override
	public void deleteTask(String storyid, String taskid) {
		tasks.get(storyid).remove(taskid);
	}

	@Override
	public void moveTask(String storyid, String taskid, SrumEnum column) {
		tasks.get(storyid).get(taskid).setColumn(column);
	}

	@Override
	public void updateTask(String storyid, String taskid, String description) {
		tasks.get(storyid).get(taskid).setDescription(description);
	}

	@Override
	public void close() throws IOException {
		FileOutputStream fout = new FileOutputStream(path);
		ObjectOutputStream oos = new ObjectOutputStream(fout);
		oos.writeObject(this);
		oos.close();
		fout.close();
	}

	@Override
	public void init(boolean reset) throws SQLException {
	}
}
