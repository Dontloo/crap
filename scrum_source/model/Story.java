package model;

import java.io.Serializable;

import parser.SrumEnum;

/**
 * @author Donglu
 * model class for stories
 */
public class Story implements Serializable{
	
	private static final long serialVersionUID = 3037264879720847738L;
	private String storyID;
	private String description;
	private SrumEnum status;
	
	public Story(String argStoryID, String argDescription, SrumEnum argStatus){
		storyID = argStoryID;
		description = argDescription;
		status = argStatus;			
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public SrumEnum getStatus() {
		return status;
	}
	public void setStatus(SrumEnum status) {
		this.status = status;
	}

	public String getStoryID() {
		return storyID;
	}

	public void setStoryID(String storyID) {
		this.storyID = storyID;
	}

}
