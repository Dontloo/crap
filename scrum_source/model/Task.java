package model;

import java.io.Serializable;

import parser.SrumEnum;

/**
 * @author Donglu
 * model class for tasks
 */
public class Task implements Serializable{
	
	private static final long serialVersionUID = 727150625556199856L;
	private String storyID;
	private String taskID;
	private String description;
	private SrumEnum column;
	
	public Task(String argStoryID, String argTaskID, String argDescription, SrumEnum argColumn){
		taskID = argTaskID;
		description = argDescription;
		setColumn(argColumn);
		storyID = argStoryID;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStoryID() {
		return storyID;
	}
	public void setStoryID(String storyID) {
		this.storyID = storyID;
	}

	public String getTaskID() {
		return taskID;
	}

	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	public SrumEnum getColumn() {
		return column;
	}

	public void setColumn(SrumEnum column) {
		this.column = column;
	}	
	
}
