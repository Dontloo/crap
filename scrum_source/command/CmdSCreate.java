package command;

import java.io.PrintStream;

import parser.SrumEnum;
import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * create story if story ID is valid,
 * the status of a story is initialized to be ACTIVE.
 */
public class CmdSCreate implements Command {
	
	private String name;
	private String ID;
	private String description;
	
	public CmdSCreate(String argID, String argDescription, String argName){
		name = argName;
		ID = argID;
		description = argDescription;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(ID)!=null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		// the status of a story is initialized to be ACTIVE.
		dataSource.createStory(ID, description, SrumEnum.ACTIVE);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+ID+": "+description;		
	}

}
