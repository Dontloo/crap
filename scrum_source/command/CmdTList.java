package command;

import java.io.PrintStream;

import model.Task;
import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * list tasks if story ID is valid
 */
public class CmdTList implements Command {
	
	private String name;
	private String storyID;
	
	public CmdTList(String argStoryID, String argName){
		name = argName;
		storyID = argStoryID;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(storyID)==null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		for(Task task:dataSource.getTasks(storyID)){
			out.print("Story ID: "+task.getStoryID());
			out.print("\tTask ID: "+task.getTaskID());
			out.print("\tDescription: "+task.getDescription());
			out.println("\tColumn: "+task.getColumn());
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+storyID;		
	}

}
