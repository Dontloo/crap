package command;

import java.io.PrintStream;

import model.Task;
import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * delete stories and all its tasks if story ID is valid
 */
public class CmdSDelete implements Command {
	
	private String name;
	private String storyID;
	
	public CmdSDelete(String argStoryID, String argName){
		name = argName;
		storyID = argStoryID;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(storyID)==null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		for(Task task: dataSource.getTasks(storyID)){
			dataSource.deleteTask(storyID, task.getTaskID());
		}		
		dataSource.deleteStory(storyID);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+storyID;		
	}

}
