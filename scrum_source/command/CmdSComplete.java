package command;

import java.io.PrintStream;

import parser.SrumEnum;
import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * complete a story if story ID is valid
 */
public class CmdSComplete implements Command {
	
	private String name;
	private String ID;
	
	public CmdSComplete(String argID, String argName){
		name = argName;
		ID = argID;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(ID)==null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		dataSource.updateStory(ID, SrumEnum.COMPLETE);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+ID;		
	}

}
