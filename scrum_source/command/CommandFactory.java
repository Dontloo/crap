package command;

import java.util.List;

import parser.SrumEnum;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * for creating commands according to rules in the grammar
 */
public class CommandFactory {
	
	/**
	 * create a command according to a given rule in the grammar (specified by the id parameter)
	 * @throws ScrumException if no corresponding commands are available
	 */
	public static Command createCmd(int id, List<String> tokens) throws ScrumException{
		String cmdName = (tokens.get(0)+" "+tokens.get(1)).toLowerCase();
		switch (id) {
		case 0:	return new CmdSCreate(tokens.get(2), tokens.get(3), cmdName);
		case 1:	return new CmdSList(cmdName);
		case 2:	return new CmdSDelete(tokens.get(2), cmdName);
		case 3:	return new CmdSComplete(tokens.get(2), cmdName);
		case 4:	return new CmdTCreate(tokens.get(2), tokens.get(3), tokens.get(4), cmdName);
		case 5:	return new CmdTList(tokens.get(2), cmdName);
		case 6:	return new CmdTDelete(tokens.get(2), tokens.get(3), cmdName);
		case 7:	return new CmdTMove(tokens.get(2), tokens.get(3), SrumEnum.valueOf(tokens.get(4).toUpperCase()), cmdName);
		case 8:	return new CmdTUpdate(tokens.get(2), tokens.get(3), tokens.get(4), cmdName);
		}		
		throw new ScrumException(ExcptionType.CMD_NOT_FOUND);
	}
}
