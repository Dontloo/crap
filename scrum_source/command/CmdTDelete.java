package command;

import java.io.PrintStream;

import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * delete task if task ID and story ID are valid
 */
public class CmdTDelete implements Command {
	
	private String name;
	private String storyID;
	private String taskID;
	
	public CmdTDelete(String argStoryID, String argTaskID, String argName){
		name = argName;
		storyID = argStoryID;
		taskID = argTaskID;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(storyID)==null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		if(dataSource.getTask(storyID,taskID)==null){
			throw new ScrumException(ExcptionType.INVALID_TASK_NAME);
		}
		dataSource.deleteTask(storyID, taskID);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+storyID+": "+taskID;		
	}

}
