package command;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import parser.SrumEnum;
import model.Task;
import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * move task if story ID and task ID are valid,
 * and move is allowed by the transition graph.
 */
public class CmdTMove implements Command {
	
	private String name;
	private String storyID;
	private String taskID;
	private SrumEnum column;
	private Map<SrumEnum,Set<SrumEnum>> graph; //directed transition graph
	
	public CmdTMove(String argStoryID, String argTaskID, SrumEnum argColumn, String argName){
		name = argName;
		storyID = argStoryID;
		taskID = argTaskID;
		column = argColumn;
		
		// create a transition graph,
		// moves are only allowed when there exists an edge from the current column to the target column.
		graph = new HashMap<SrumEnum,Set<SrumEnum>>();
		Set<SrumEnum> edges;
		// SrumEnum.TO_DO
		edges = new HashSet<SrumEnum>();
		edges.add(SrumEnum.TO_DO);
		edges.add(SrumEnum.IN_PROCESS);
		graph.put(SrumEnum.TO_DO, edges);
		// SrumEnum.IN_PROCESS
		edges = new HashSet<SrumEnum>();
		edges.add(SrumEnum.TO_DO);
		edges.add(SrumEnum.IN_PROCESS);
		edges.add(SrumEnum.TO_VERIFY);
		graph.put(SrumEnum.IN_PROCESS, edges);
		// SrumEnum.TO_VERIFY
		edges = new HashSet<SrumEnum>();
		edges.add(SrumEnum.TO_DO);
		edges.add(SrumEnum.IN_PROCESS);
		edges.add(SrumEnum.TO_VERIFY);
		edges.add(SrumEnum.DONE);
		graph.put(SrumEnum.TO_VERIFY, edges);
		// SrumEnum.DONE
		edges = new HashSet<SrumEnum>();
		edges.add(SrumEnum.DONE);
		graph.put(SrumEnum.DONE, edges);
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(storyID)==null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		Task task = dataSource.getTask(storyID,taskID);
		if(task==null){
			throw new ScrumException(ExcptionType.INVALID_TASK_NAME);
		}		
		if(!graph.get(task.getColumn()).contains(column)){
			throw new ScrumException(ExcptionType.INVALID_TASK_COLUMN);
		}
		dataSource.moveTask(storyID, taskID, column);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+storyID+": "+taskID+": "+column;		
	}

}
