package command;

import java.io.PrintStream;

import parser.SrumEnum;
import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * create task if story ID and task ID are valid,
 * the status of a story is initialized to be TO_DO.
 */
public class CmdTCreate implements Command {
	
	private String name;
	private String storyID;
	private String taskID;
	private String description;
	
	public CmdTCreate(String argStoryID, String argTaskID, String argDescription, String argName){
		name = argName;
		storyID = argStoryID;
		taskID = argTaskID;
		description = argDescription;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(storyID)==null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		if(dataSource.getTask(storyID,taskID)!=null){
			throw new ScrumException(ExcptionType.INVALID_TASK_NAME);
		}
		dataSource.createTask(storyID, taskID, description, SrumEnum.TO_DO);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+storyID+": "+taskID+": "+description;		
	}

}
