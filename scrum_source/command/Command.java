package command;

import java.io.PrintStream;

import datasourse.DataSource;
import execption.ScrumException;

/**
 * @author Donglu
 * command doesn't need to know data source and output stream upon creation, only when it's executed.
 */
public interface Command {
	
	/**
	 * execute the command on a given data source and output stream
	 * @throws ScrumException if there is a runtime error.
	 */
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException;
	
	/**
	 * for testing
	 */
	public String toString();
}
