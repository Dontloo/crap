package command;

import java.io.PrintStream;

import model.Story;
import datasourse.DataSource;
import execption.ScrumException;

/**
 * @author Donglu
 * display stories
 */
public class CmdSList implements Command {
	
	private String name;
	
	public CmdSList(String argName){
		name = argName;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		for(Story story:dataSource.getStories()){
			out.print("Story ID: "+story.getStoryID());
			out.print("\tDescription: "+story.getDescription());
			out.println("\tStatus: "+story.getStatus());
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name;		
	}

}
