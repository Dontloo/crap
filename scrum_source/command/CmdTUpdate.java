package command;

import java.io.PrintStream;

import datasourse.DataSource;
import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * update task if story ID and task ID are valid.
 */
public class CmdTUpdate implements Command {
	
	private String name;
	private String storyID;
	private String taskID;
	private String description;
	
	public CmdTUpdate(String argStoryID, String argTaskID, String argDescription, String argName){
		name = argName;
		storyID = argStoryID;
		taskID = argTaskID;
		description = argDescription;
	}
	
	/* (non-Javadoc)
	 * @see command.Command#execute(datasourse.DataSource, java.io.PrintStream)
	 */
	@Override
	public void execute(DataSource dataSource, PrintStream out) throws ScrumException{
		if(dataSource.getStory(storyID)==null){
			throw new ScrumException(ExcptionType.INVALID_STORY_NAME);
		}
		if(dataSource.getTask(storyID,taskID)==null){
			throw new ScrumException(ExcptionType.INVALID_TASK_NAME);
		}
		dataSource.updateTask(storyID, taskID, description);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return name+": "+storyID+": "+taskID+": "+description;		
	}

}
