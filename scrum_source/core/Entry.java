package core;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

import parser.ScrumGrammar;
import datasourse.Mode;
import execption.ScrumException;


/**
 * @author Donglu
 * entry of the command line scrum task board program.
 * type command line EOF to terminate (Unix: ctrl-D Win: ctrl-Z)
 */
public class Entry {
	/* sample test case:

	create story s1 this is a happy story.
	create story s2 this is a sad story.
	list stories
	create task s1 t1 this is an endless task.
	create task s2 t1 this is an endless task.
	list tasks s1
	list tasks s2
	
	list tasks s1
	move task s1 t1 in_process
	list tasks s1
	move task s1 t1 to_verify
	list tasks s1
	move task s1 t1 done
	list tasks s1
	move task s2 t1 in_process
	move task s2 t1 to_verify
	list tasks s2
	move task s2 t1 in_process
	list tasks s2
	move task s2 t1 to_do
	list tasks s2
	move task s2 t1 in_process
	move task s2 t1 to_verify
	list tasks s2
	move task s2 t1 to_do
	list tasks s2
	
	update task s1 t1 this task has ended.
	list tasks s1
	
	delete task s1 t1
	list tasks s1
	
	complete story s1
	list stories
	
	delete story s1
	list stories
	 */
	/**
	 * @param args
	 * read data source settings for "scrum_config" file.
	 * it uses Derby database by default, 
	 * prints output to Standard Output Library (System.out),
	 * and uses the grammar specified in the ScrumGrammar class.
	 * @throws SQLException 
	 * @throws ScrumException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * these exceptions are caused by invalid configuration file,
	 * or problems of accessing the file system and databases, 
	 * can't do much.
	 */
	public static void main(String[] args) throws ClassNotFoundException, IOException, ScrumException, SQLException{
		// read configuration file
		Scanner sc = new Scanner(new File("scrum_config"));
		String datasourceType = sc.nextLine();
		String url = sc.nextLine();
		Mode mode = Mode.valueOf(sc.nextLine());
		sc.close();
		
		Scrum scrum = new Scrum(datasourceType,url,mode,System.out,new ScrumGrammar());
		
		// read user input
		sc = new Scanner(System.in);
		while(sc.hasNext()){
			scrum.processLine(sc.nextLine());
		}
		sc.close();
		scrum.close();
	}

}
