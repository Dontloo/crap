package core;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;

import parser.ScrumGrammar;
import parser.Parser;

import command.Command;

import datasourse.DataSource;
import datasourse.DataSourceFactory;
import datasourse.Mode;
import execption.ScrumException;

/**
 * @author Donglu
 * the scrum task board.
 * parse the input with a specified grammar,
 * persist the information in a specified data source,
 * display the output on a specified output stream.
 */
public class Scrum {
	
	private Parser parser;
	private DataSource dataSource;
	private PrintStream printStream;
	
	/**
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ScrumException
	 * these exceptions are caused by invalid configuration file,
	 * or problems of accessing the file system and databases, 
	 * can't do much.
	 */
	public Scrum(String type, String url, Mode mode, PrintStream argPrintStream, ScrumGrammar argRules) throws ClassNotFoundException, IOException, SQLException, ScrumException{
		parser = new Parser(argRules);
		dataSource = DataSourceFactory.createDataSource(type,url,mode);
		printStream = argPrintStream;
	}	

	/**
	 * @param line
	 * parse the input into command
	 * execute the command on data source and output stream
	 * if success, output 0
	 * if not, output 1
	 */
	public void processLine(String line){
		// non-empty line
		if(line.trim().length()!=0){
			try {
				Command cmd = parser.parseLine(line);
				cmd.execute(dataSource, printStream);
				printStream.println("0");
			} catch (ScrumException e) {
				// can display detail error information via e.getExcptionType()
				printStream.println("1");
			}
		}
	}

	public void close() throws IOException, SQLException {
		printStream.close();
		dataSource.close();				
	}

}
