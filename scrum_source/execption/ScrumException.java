package execption;

/**
 * @author Donglu
 * for encapsulating known exceptions
 */
public class ScrumException extends Exception {

	private static final long serialVersionUID = -121034449205512342L;
	private ExcptionType eType;

	public ScrumException(ExcptionType argEType) {
		super();
		eType = argEType;
	}

	/**
	 * for testing
	 */
	public ExcptionType getExcptionType() {
		return eType;
	}

}