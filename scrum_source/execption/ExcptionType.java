package execption;

/**
 * @author Donglu
 * enumeration for exception types
 */
public enum ExcptionType {
	PARSING_ERR, CMD_NOT_FOUND, CMD_STATUS_NOT_FOUND, INVALID_STORY_NAME, INVALID_TASK_NAME, INVALID_TASK_COLUMN, DATASOURCE_NOT_FOUND, SQL_EXCEPTION, MAXIMUM_ID_LEN, MAXIMUM_DES_LEN
}