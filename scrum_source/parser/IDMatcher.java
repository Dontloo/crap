package parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Donglu
 * matcher for any one or more non-whitespace characters
 * (used for story ID and taks ID)
 */
public class IDMatcher implements ScrumMatcher{
	// one or more non-whitespace characters
    private static final String REGEX = "\\S+";
    private Pattern pattern;
    
    public IDMatcher(){
    	pattern = Pattern.compile(REGEX);
    }

	/* (non-Javadoc)
	 * @see parser.ScrumMatcher#match(java.lang.String)
	 */
	@Override
	public MatchResult match(String str) {
		Matcher m = pattern.matcher(str);
		if (m.find()) {
			return new MatchResult(m.group(), str.substring(m.end()));
		}
		return null;
	}
	
	/**
	 * for testing
	 */
    public static void main( String args[] ){
    	Pattern p = Pattern.compile(REGEX);
    	String str = " create story my_story this is a sad story.";
    	Matcher m = p.matcher(str); // get a matcher object

		if (m.find()) {
			System.out.println("start(): " + m.start());
			System.out.println("end(): " + m.end());
			System.out.println(m.group());
			System.out.println(str.substring(m.end()));
		}
   }


}
