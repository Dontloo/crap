package parser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Donglu
 * matcher for a set of strings (used for task column)
 * e.g. to_do/in_process/to_verify/done
 */
public class ColumnMatcher implements ScrumMatcher{
	// one or more non-whitespace characters
    private static final String REGEX = "\\S+";
    private Pattern pattern;
    private Set<String> targets;
    
    public ColumnMatcher(String[] strs){
    	pattern = Pattern.compile(REGEX);
		targets = new HashSet<String>(Arrays.asList(strs));
    }
    
	/* (non-Javadoc)
	 * @see parser.ScrumMatcher#match(java.lang.String)
	 */
	@Override
	public MatchResult match(String str){
		Matcher m = pattern.matcher(str);
		if (m.find()) {
			// all to upper case in accordance with the name in the Status enumeration
			if(targets.contains(m.group().toUpperCase())){
				return new MatchResult(m.group(), str.substring(m.end()));
			}			
		}
		return null;
	}

}
