package parser;

import java.util.ArrayList;
import java.util.List;

import command.Command;
import command.CommandFactory;

import execption.ExcptionType;
import execption.ScrumException;

/**
 * @author Donglu
 * parse input into commands according to a given grammar.
 */
public class Parser {
	
	private List<List<ScrumMatcher>> rules;

	public Parser(ScrumGrammar argRules){
		rules = argRules.getRules();
	}

	/**
	 * will return the fist command matched according to the order they are specified in the grammar,
	 * @throws ScrumException if no rules are satisfied.
	 */
	public Command parseLine(String line) throws ScrumException {
		for(int i=0; i<rules.size();i++){
			Command cmd = matchCmd(rules.get(i),line, i);
			if(cmd!=null){
				return cmd;
			}
		}
		throw new ScrumException(ExcptionType.PARSING_ERR);	
	}
	
	/**
	 * try to match the input to a given rule in the grammar,
	 * if doesn't match return null.
	 * @throws ScrumException if the input satisfies the grammar but no corresponding commands are found. 
	 */
	private Command matchCmd(List<ScrumMatcher> rule, String line, int id) throws ScrumException {
		// a rule is satisfied if all the ScrumMatcher classes are matched subsequently,
		// and no non-whitespace characters are left.
		List<String> pieces = new ArrayList<String>();
		for(ScrumMatcher matcher : rule){
			MatchResult res = matcher.match(line);
			// if any 'piece' doesn't match, return null
			if(res==null){
				return null;
			}else{
				line = res.getRemain();
				pieces.add(res.getMatch());
			}
		}
		// if there are non-whitespace characters left, return null
		if(line.trim().length()!=0){
			return null;
		}		
		return CommandFactory.createCmd(id, pieces);
	}

	/**
	 * for testing
	 */
	public static void main(String[] args) {
		Parser parser = new Parser(new ScrumGrammar());
		try {
			Command cmd = parser.parseLine("	creAte    stOry    my_story this is a sad story.");
			System.out.println(cmd.toString());
			cmd = parser.parseLine("	moVe    taSk     my_story  task_1  in_process  ");
			System.out.println(cmd.toString());
		} catch (ScrumException e) {
			System.out.println(e.getExcptionType());
		}		
	}

}
