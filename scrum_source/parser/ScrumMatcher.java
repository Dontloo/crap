package parser;

public interface ScrumMatcher {
	/**
	 * if matches, return the matched string and the remaining string,
	 * will return null if can not find a match
	 */
	public MatchResult match(String str);
}