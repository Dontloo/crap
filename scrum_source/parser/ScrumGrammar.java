package parser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Donglu
 * a rule in the grammar consists of a list of ScrumMatcher classes.
 * a rule is satisfied if all the ScrumMatcher classes are matched subsequently,
 * and no non-whitespace characters are left.
 */
public class ScrumGrammar {
	
	private List<List<ScrumMatcher>> rules;
	
	/**
	 * key words should be all in lower case
	 */
	public ScrumGrammar(){
		rules = new ArrayList<List<ScrumMatcher>>();
		List<ScrumMatcher> rule;
		// rule #1
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("create"));
		rule.add(new InstructionMatcher("story"));
		rule.add(new IDMatcher());
		rule.add(new DescriptionMatcher());
		rules.add(rule);
		// rule #2
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("list"));
		rule.add(new InstructionMatcher("stories"));
		rules.add(rule);
		// rule #3
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("delete"));
		rule.add(new InstructionMatcher("story"));
		rule.add(new IDMatcher());
		rules.add(rule);
		// rule #4
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("complete"));
		rule.add(new InstructionMatcher("story"));
		rule.add(new IDMatcher());
		rules.add(rule);
		// rule #5
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("create"));
		rule.add(new InstructionMatcher("task"));
		rule.add(new IDMatcher());
		rule.add(new IDMatcher());
		rule.add(new DescriptionMatcher());
		rules.add(rule);
		// rule #6
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("list"));
		rule.add(new InstructionMatcher("tasks"));
		rule.add(new IDMatcher());
		rules.add(rule);
		// rule #7
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("delete"));
		rule.add(new InstructionMatcher("task"));
		rule.add(new IDMatcher());
		rule.add(new IDMatcher());
		rules.add(rule);
		// rule #8
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("move"));
		rule.add(new InstructionMatcher("task"));
		rule.add(new IDMatcher());
		rule.add(new IDMatcher());
		rule.add(new ColumnMatcher(new String[]{SrumEnum.TO_DO.toString(), SrumEnum.IN_PROCESS.toString(), SrumEnum.TO_VERIFY.toString(), SrumEnum.DONE.toString()}));
		rules.add(rule);
		// rule #9
		rule = new ArrayList<ScrumMatcher>();
		rule.add(new InstructionMatcher("update"));
		rule.add(new InstructionMatcher("task"));
		rule.add(new IDMatcher());
		rule.add(new IDMatcher());
		rule.add(new DescriptionMatcher());
		rules.add(rule);
	}
	
	public List<List<ScrumMatcher>> getRules() {
		return rules;
	}
}
