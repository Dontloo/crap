package parser;

/**
 * @author Donglu
 * for ScrumMatcher to return matching results
 */
public class MatchResult {
	
	private String match; // the string matched
	private String remain;// the remaining string
	
	public MatchResult(String argMatch, String argRemain){
		this.match = argMatch;
		this.remain = argRemain;
	}
	
	public String getMatch() {
		return match;
	}
	public String getRemain() {
		return remain;
	}
	
}
