package parser;

/**
 * @author Donglu
 * enumeration of some constant values specified by the grammar
 */
public enum SrumEnum {
	COMPLETE, ACTIVE, TO_DO, IN_PROCESS, TO_VERIFY, DONE
}