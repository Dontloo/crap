package parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Donglu
 * matcher for a specific string (used for key words)
 */
public class InstructionMatcher implements ScrumMatcher{
	// one or more non-whitespace characters
    private static final String REGEX = "\\S+";
    private Pattern pattern;
    private String target;
    
	public InstructionMatcher(String str){
		target = str;
		pattern = Pattern.compile(REGEX);
	}

	/* (non-Javadoc)
	 * @see parser.ScrumMatcher#match(java.lang.String)
	 */
	@Override
	public MatchResult match(String str) {
		Matcher m = pattern.matcher(str);
		if (m.find()) {
			// case insensitive
			if(target.equalsIgnoreCase(m.group())){
				return new MatchResult(m.group(), str.substring(m.end()));
			}			
		}
		return null;
	}
	/**
	 * for testing
	 */
	public static void main(String[] args) {
		String str = " story my_story this is a sad story.";
		InstructionMatcher m = new InstructionMatcher("story");
		MatchResult res = m.match(str);
		System.out.println(res.getMatch());
	}

}
