package parser;

/**
 * @author Donglu
 * matcher for any string (used for description)
 */
public class DescriptionMatcher implements ScrumMatcher{    
	/* (non-Javadoc)
	 * @see parser.ScrumMatcher#match(java.lang.String)
	 */
	@Override
	public MatchResult match(String str){
		return new MatchResult(str.trim(), "");
	}
}
